FROM python:3.5

RUN apt-get update && apt-get install -y python-pip

ENV PYTHONPATH=/src/flaskr
ENV FLASK_APP=flaskr/flaskr.py

ADD flaskr /src/flaskr/ 

RUN cd /src/flaskr/ && pip install --editable . && flask initdb

EXPOSE 5000

WORKDIR /src/flaskr/

CMD flask run
